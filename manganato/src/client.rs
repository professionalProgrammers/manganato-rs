use crate::Chapter;
use crate::Error;
use crate::Manga;
use crate::SearchResults;
use reqwest::header::HeaderMap;
use reqwest::header::HeaderValue;
use scraper_util::FromHtml;
use scraper_util::Html;
use std::sync::Arc;
use tokio::sync::Semaphore;

static REFERER_VALUE: HeaderValue = HeaderValue::from_static("https://manganato.com");

/// Client
#[derive(Debug, Clone)]
pub struct Client {
    /// The inner http client
    ///
    /// Probably should not be used directly
    pub client: reqwest::Client,

    /// The request limiter, which limits the number of in-flight requests.
    ///
    /// Defaults to 64.
    pub request_semaphore: Arc<Semaphore>,
}

impl Client {
    /// Make a new [`Client`].
    pub fn new() -> Self {
        let mut default_headers = HeaderMap::new();
        default_headers.insert(reqwest::header::REFERER, REFERER_VALUE.clone());

        let client = reqwest::Client::builder()
            .default_headers(default_headers)
            .build()
            .expect("failed to build manganato reqwest client");

        let request_semaphore = Arc::new(Semaphore::new(64));

        Self {
            client,
            request_semaphore,
        }
    }

    /// Send a GET to a url and get the response as text.
    pub(crate) async fn get_text(&self, url: &str) -> Result<String, Error> {
        let _permit = self
            .request_semaphore
            .acquire()
            .await
            .expect("semaphore closed");
        let text = self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;
        Ok(text)
    }

    /// Send a GET to a url and get the response as html, parsing it with the `FromHtml` trait.
    pub(crate) async fn get_html<T>(
        &self,
        url: &str,
    ) -> Result<Result<T, scraper_util::FromHtmlError>, Error>
    where
        T: FromHtml + Send + 'static,
    {
        let text = self.get_text(url).await?;
        let ret = tokio::task::spawn_blocking(move || {
            let html = Html::parse_document(&text);
            T::from_html(&html)
        })
        .await?;
        Ok(ret)
    }

    /// Search with a query
    pub async fn search(&self, raw_query: &str) -> Result<SearchResults, Error> {
        let query = make_query(raw_query);
        let url = format!("https://manganato.com/search/story/{query}");
        Ok(self.get_html(url.as_str()).await??)
    }

    /// Get a manga by url
    pub async fn get_manga(&self, url: &str) -> Result<Manga, Error> {
        Ok(self.get_html(url).await??)
    }

    /// Get a chapter by url
    pub async fn get_chapter(&self, url: &str) -> Result<Chapter, Error> {
        Ok(self.get_html(url).await??)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

fn make_query(raw_query: &str) -> String {
    fn add_space_if_needed(s: &mut String) {
        if s.chars().rev().next().map_or(true, |c| c != '_') {
            s.push('_')
        }
    }

    let mut query = String::with_capacity(raw_query.len());
    for c in raw_query.chars() {
        match c {
            ' ' => add_space_if_needed(&mut query),
            'à' | 'á' | 'ạ' | 'ả' | 'ã' | 'â' | 'ầ' | 'ấ' | 'ậ' | 'ẩ' | 'ẫ' | 'ă' | 'ằ' | 'ắ'
            | 'ặ' | 'ẳ' | 'ẵ' => query.push('a'),
            'è' | 'é' | 'ẹ' | 'ẻ' | 'ẽ' | 'ê' | 'ề' | 'ế' | 'ệ' | 'ể' | 'ễ' => {
                query.push('e')
            }
            'ì' | 'í' | 'ị' | 'ỉ' | 'ĩ' => query.push('i'),
            'ò' | 'ó' | 'ọ' | 'ỏ' | 'õ' | 'ô' | 'ồ' | 'ố' | 'ộ' | 'ổ' | 'ỗ' | 'ơ' | 'ờ' | 'ớ'
            | 'ợ' | 'ở' | 'ỡ' => query.push('o'),
            'ù' | 'ú' | 'ụ' | 'ủ' | 'ũ' | 'ư' | 'ừ' | 'ứ' | 'ự' | 'ử' | 'ữ' => {
                query.push('u')
            }
            'đ' => query.push('d'),
            'ỳ' | 'ý' | 'ỵ' | 'ỷ' | 'ỹ' => query.push('y'),
            c => {
                let c = c.to_ascii_lowercase();
                if c.is_ascii_digit() || c.is_ascii_lowercase() {
                    query.push(c)
                } else {
                    add_space_if_needed(&mut query)
                }
            }
        }
    }
    query
}

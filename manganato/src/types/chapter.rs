use url::Url;

/// A manga chapter page
#[derive(Debug, Clone, scraper_util::FromHtml)]
pub struct Chapter {
    /// Chapter images
    #[scraper_util(selector = ".container-chapter-reader > img")]
    pub images: Vec<Image>,
}

#[derive(Debug, Clone, scraper_util::FromElement)]
pub struct Image {
    /// The image title
    #[scraper_util(attribute = "title")]
    pub title: String,

    /// The image url
    #[scraper_util(attribute = "src")]
    pub url: Url,
}

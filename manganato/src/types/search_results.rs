use url::Url;

/// Search Results
#[derive(Debug, scraper_util::FromHtml)]
pub struct SearchResults {
    /// Search Entries
    #[scraper_util(selector = ".panel-search-story > .search-story-item")]
    pub entries: Vec<SearchEntry>,
}

/// Search Entry
#[derive(Debug, scraper_util::FromElement)]
pub struct SearchEntry {
    /// Entry title
    #[scraper_util(selector = "h3 > a", text)]
    pub title: String,

    /// The entry url
    #[scraper_util(selector = "h3 > a", attribute = "href")]
    pub url: Url,
}

use url::Url;

/// A Manga page
#[derive(Debug, Clone, scraper_util::FromHtml)]
pub struct Manga {
    /// The title
    #[scraper_util(selector = ".story-info-right > h1", text)]
    pub title: String,

    /// Chapters
    #[scraper_util(selector = ".row-content-chapter > li")]
    pub chapters: Vec<Chapter>,
}

/// A manga chapter
#[derive(Debug, Clone, scraper_util::FromElement)]
pub struct Chapter {
    /// The chapter title
    #[scraper_util(selector = "a", text)]
    pub title: String,

    /// The chapter url
    #[scraper_util(selector = "a", attribute = "href")]
    pub url: Url,
}

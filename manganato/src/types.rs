/// The [`Chapter`] type.
mod chapter;
/// The [`Manga`] type.
mod manga;
/// The [`SearchResults`] type.
mod search_results;

pub use self::chapter::Chapter;
pub use self::chapter::Image as ChapterImage;
pub use self::manga::Chapter as MangaChapter;
pub use self::manga::Manga;
pub use self::search_results::SearchResults;

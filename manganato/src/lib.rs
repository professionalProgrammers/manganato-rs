/// The client
mod client;
/// API types
mod types;

pub use crate::client::Client;
pub use crate::types::Chapter;
pub use crate::types::ChapterImage;
pub use crate::types::Manga;
pub use crate::types::MangaChapter;
pub use crate::types::SearchResults;
pub use url::Url;

/// Error type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// HTTP error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    /// Join Error
    #[error(transparent)]
    TokioError(#[from] tokio::task::JoinError),

    #[error("failed to extract from html")]
    Html(#[from] scraper_util::FromHtmlError),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn search_works() {
        let client = Client::new();
        let res = client.search("maid").await.expect("failed to search");
        dbg!(res);
    }

    #[tokio::test]
    async fn get_manga_works() {
        // let url = "https://manganato.com/manga-fb983084";
        let url = "https://readmanganato.com/manga-hk984567";
        let client = Client::new();
        let manga = client.get_manga(url).await.expect("failed to get manga");
        dbg!(&manga);
        assert!(manga.title == "Meika-San Can't Conceal Her Emotions");
    }

    #[tokio::test]
    async fn get_chapter_works() {
        // let url = "https://manganato.com/manga-fb983084/chapter-1";
        let url = "https://readmanganato.com/manga-hk984567/chapter-1";
        let client = Client::new();
        let res = client
            .get_chapter(url)
            .await
            .expect("failed to get chapter");
        dbg!(res);
    }
}

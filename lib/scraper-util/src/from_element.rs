use crate::FromAttributeError;
use crate::FromTextError;
use scraper::ElementRef;

/// An error that may occur while extracting a struct from a series of elements via [`FromElement`].
#[derive(Debug)]
pub enum FromElementError {
    /// The element is missing
    Missing(String),

    /// An error occured while processing attributes
    Attribute(FromAttributeError),

    /// An error occured while processing an element.
    ///
    /// This corresponds with a nested call to `select` while extracting an element.
    Element(Box<Self>),

    /// An error occured while processing text
    Text(FromTextError),
}

impl std::fmt::Display for FromElementError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Missing(selector) => write!(f, "missing element with selector \"{selector}\""),
            Self::Attribute(_error) => write!(f, "attribute error"),
            Self::Element(_error) => write!(f, "element error"),
            Self::Text(_error) => write!(f, "text error"),
        }
    }
}

impl std::error::Error for FromElementError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Attribute(error) => Some(error),
            Self::Element(error) => Some(error),
            Self::Text(error) => Some(error),
            _ => None,
        }
    }
}

impl From<FromAttributeError> for FromElementError {
    fn from(error: FromAttributeError) -> Self {
        Self::Attribute(error)
    }
}

impl From<FromTextError> for FromElementError {
    fn from(error: FromTextError) -> Self {
        Self::Text(error)
    }
}

/// Implemented for types than can be parsed from a series of HTML elements.
///
/// Structs that only support one element can implement this trait
/// by only taking the first element from the iterator.
///
/// Structs that optionally take an element can also implement this trait
/// by handling an empty iterator.
pub trait FromElement {
    /// Parse this struct from a HTML element.
    fn from_element<'a>(
        selector: &str,
        elements: impl Iterator<Item = ElementRef<'a>>,
    ) -> Result<Self, FromElementError>
    where
        Self: Sized;
}

impl<T> FromElement for Vec<T>
where
    T: FromElement,
{
    fn from_element<'a>(
        selector: &str,
        elements: impl Iterator<Item = ElementRef<'a>>,
    ) -> Result<Self, FromElementError> {
        elements
            .map(|element| T::from_element(selector, std::iter::once(element)))
            .collect()
    }
}

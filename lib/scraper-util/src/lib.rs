/// The [`FromAttribute`] trait
mod from_attribute;
/// The [`FromElement`] trait
mod from_element;
/// The [`FromHtml`] trait
mod from_html;
/// The [`FromText`] trait
mod from_text;

pub use self::from_attribute::FromAttribute;
pub use self::from_attribute::FromAttributeError;
pub use self::from_element::FromElement;
pub use self::from_element::FromElementError;
pub use self::from_html::FromHtml;
pub use self::from_html::FromHtmlError;
pub use self::from_text::FromText;
pub use self::from_text::FromTextError;
pub use once_cell::sync::Lazy;
pub use scraper::ElementRef;
pub use scraper::Html;
pub use scraper::Selector;
pub use scraper_util_derive::FromElement;
pub use scraper_util_derive::FromHtml;

#[cfg(test)]
mod test {
    use super::*;

    const BLOG_HTML: &str = include_str!("../test_data/blog.html");

    #[test]
    fn parse_blog_links() {
        #[derive(Debug, PartialEq, Eq)]
        struct BlogPage {
            links: Vec<Link>,
        }

        impl FromHtml for BlogPage {
            fn from_html(html: &Html) -> Result<Self, FromHtmlError> {
                static LINK_SELECTOR: Lazy<Selector> = Lazy::new(|| Selector::parse("a").unwrap());

                let mut iter = html.select(&LINK_SELECTOR);
                let mut links = Vec::with_capacity(32);
                while let Some(item) = iter.next() {
                    let item = Link::from_element("a", std::iter::once(item))?;
                    links.push(item);
                }

                Ok(Self { links })
            }
        }

        #[derive(Debug, PartialEq, Eq)]
        struct Link {
            href: String,
        }

        impl FromElement for Link {
            fn from_element<'a>(
                selector: &str,
                mut elements: impl Iterator<Item = ElementRef<'a>>,
            ) -> Result<Self, FromElementError> {
                let element = elements
                    .next()
                    .ok_or_else(|| FromElementError::Missing(selector.into()))?;

                let href = element
                    .value()
                    .attr("href")
                    .ok_or_else(|| {
                        FromElementError::Attribute(FromAttributeError::Missing("href".into()))
                    })
                    .map(|attr| attr.to_string())?;
                Ok(Self { href })
            }
        }

        let html = Html::parse_document(BLOG_HTML);
        let blog_page = BlogPage::from_html(&html).expect("failed to parse");

        let expected_links = vec![
            Link { href: "/".into() },
            Link {
                href: "/elrs-rx-dualradio".into(),
            },
            Link {
                href: "/elrs-rx".into(),
            },
            Link {
                href: "/mercury-h7".into(),
            },
            Link {
                href: "/mercury-g4".into(),
            },
            Link {
                href: "/ph-module".into(),
            },
            Link {
                href: "/ec-module".into(),
            },
            Link {
                href: "/about".into(),
            },
            Link {
                href: "/checkout".into(),
            },
            Link {
                href: "/blog".into(),
            },
            Link {
                href: "mailto:anyleaf@anyleaf.org".into(),
            },
            Link {
                href: "/filter-design".into(),
            },
            Link {
                href: "/blog/quaternions:-a-practical-guide".into(),
            },
            Link {
                href: "/blog/expresslrs-overview".into(),
            },
            Link {
                href: "/blog/estimating-drone-attitude".into(),
            },
            Link {
                href: "/blog/rust-embedded-ecosystem-and-tools".into(),
            },
            Link {
                href: "/blog/quadcopter-flight-controller-mcu-comparison".into(),
            },
            Link {
                href: "/blog/parts-you-need-for-a-quadcopter-in-2022".into(),
            },
            Link {
                href: "/blog/writing-embedded-firmware-using-rust".into(),
            },
            Link {
                href: "/blog/measuring-ph-on-raspberry-pi".into(),
            },
            Link {
                href: "/blog/the-essence-of-embedded-computers".into(),
            },
            Link {
                href: "/blog/electrical-conductivity-(ec)-for-hydroponics".into(),
            },
            Link {
                href: "/blog/project:-building-an-automatic-ph-doser".into(),
            },
            Link {
                href: "/blog/ph-measurement-for-hydroponics".into(),
            },
            Link {
                href: "/blog/how-to-calibrate-ph-sensors".into(),
            },
            Link {
                href: "/blog/temperature-sensors:-a-comparison".into(),
            },
            Link {
                href: "/privacy".into(),
            },
            Link {
                href: "/terms".into(),
            },
            Link {
                href: "https://en.wikipedia.org/wiki/Restriction_of_Hazardous_Substances_Directive"
                    .into(),
            },
        ];
        let expected = BlogPage {
            links: expected_links,
        };

        assert!(expected == blog_page);
    }
}

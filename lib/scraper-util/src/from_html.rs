use crate::FromElementError;
use scraper::Html;

/// An error that may occur while extracting a struct from HTML via [`FromHtml`].
#[derive(Debug)]
pub enum FromHtmlError {
    /// An error occured while processing elements
    Element(FromElementError),
}

impl std::fmt::Display for FromHtmlError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Element(_error) => write!(f, "element error"),
        }
    }
}

impl std::error::Error for FromHtmlError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Element(error) => Some(error),
        }
    }
}

impl From<FromElementError> for FromHtmlError {
    fn from(error: FromElementError) -> Self {
        Self::Element(error)
    }
}

/// Implemented for types than can be parsed from HTML.
pub trait FromHtml {
    /// Parse this struct from HTML.
    fn from_html(html: &Html) -> Result<Self, FromHtmlError>
    where
        Self: Sized;
}

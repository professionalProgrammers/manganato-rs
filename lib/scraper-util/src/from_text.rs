/// An error that may occur while extracting a struct from a series of text nodes via [`FromText`].
#[derive(Debug)]
pub enum FromTextError {
    /// Text nodes are missing
    Missing,
}

impl std::fmt::Display for FromTextError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Missing => write!(f, "missing text nodes"),
        }
    }
}

impl std::error::Error for FromTextError {}

/// Implemented for types than can be parsed from a series of text nodes
pub trait FromText {
    fn from_text<'a>(text: impl Iterator<Item = &'a str>) -> Result<Self, FromTextError>
    where
        Self: Sized;
}

impl FromText for String {
    fn from_text<'a>(mut text: impl Iterator<Item = &'a str>) -> Result<Self, FromTextError> {
        text.next()
            .ok_or(FromTextError::Missing)
            .map(|text| text.to_string())
    }
}

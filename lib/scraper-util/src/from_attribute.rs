/// An error that may occur while extracting a struct from an attribute via [`FromAttribute`].
#[derive(Debug)]
#[non_exhaustive]
pub enum FromAttributeError {
    /// The expected attribute is missing.
    Missing(String),

    /// The expected attribute is not a url
    #[cfg(feature = "url")]
    InvalidUrl(url::ParseError),
}

impl std::fmt::Display for FromAttributeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Missing(name) => write!(f, "missing attribute \"{name}\""),

            #[cfg(feature = "url")]
            Self::InvalidUrl(_error) => write!(f, "invalid url"),
        }
    }
}

impl std::error::Error for FromAttributeError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            #[cfg(feature = "url")]
            Self::InvalidUrl(error) => Some(error),
            _ => None,
        }
    }
}

/// Implemented for types that can be parsed from a HTML element attribute.
pub trait FromAttribute {
    fn from_attribute(name: &str, value: Option<&str>) -> Result<Self, FromAttributeError>
    where
        Self: Sized;
}

impl FromAttribute for String {
    fn from_attribute(name: &str, value: Option<&str>) -> Result<Self, FromAttributeError> {
        value
            .ok_or_else(|| FromAttributeError::Missing(name.into()))
            .map(|value| value.to_string())
    }
}

#[cfg(feature = "url")]
impl FromAttribute for url::Url {
    fn from_attribute(name: &str, value: Option<&str>) -> Result<Self, FromAttributeError> {
        value
            .ok_or_else(|| FromAttributeError::Missing(name.into()))
            .map(Self::parse)?
            .map_err(FromAttributeError::InvalidUrl)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn string() {
        const STRING_STR: &str = "string";
        let string = String::from_attribute("href", Some(STRING_STR))
            .expect("failed to parse string from attribute");
        assert!(string == STRING_STR);
    }

    #[cfg(feature = "url")]
    #[test]
    fn url() {
        use url::Url;

        const URL_STR: &str = "https://docs.rs/url/latest/url/";
        let url =
            Url::from_attribute("href", Some(URL_STR)).expect("failed to parse url from attribute");
        assert!(url.as_str() == URL_STR);
    }
}

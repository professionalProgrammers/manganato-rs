use quote::format_ident;
use quote::quote;
use quote::quote_spanned;
use std::collections::HashMap;
use syn::parse_macro_input;
use syn::punctuated::Punctuated;
use syn::spanned::Spanned;
use syn::token::Comma;
use syn::Attribute;
use syn::Data;
use syn::DeriveInput;
use syn::Error;
use syn::Fields;
use syn::Ident;
use syn::Lit;
use syn::LitStr;
use syn::Meta;
use syn::NestedMeta;
use syn::Result;
use syn::Type;

#[proc_macro_derive(FromHtml, attributes(scraper_util))]
pub fn derive_from_html(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let fields = match &input.data {
        Data::Struct(data) => &data.fields,
        Data::Enum(_) | Data::Union(_) => unimplemented!("expected a struct"),
    };
    let fields = match &fields {
        Fields::Named(fields) => &fields.named,
        Fields::Unnamed(fields) => &fields.unnamed,
        Fields::Unit => unimplemented!("unit structs are unsupported"),
    };
    let fields = match get_fields(fields) {
        Ok(fields) => fields,
        Err(error) => return error.into_compile_error().into(),
    };

    let mut type_trampoline_map = HashMap::with_capacity(fields.len());
    let mut field_selector_map = HashMap::with_capacity(fields.len());

    let impl_ = {
        let recurse = fields.iter().map(|field| {
            let selector = &field.attrs.selector;
            let from = &field.attrs.from;
            let text = field.attrs.text;
            let attribute = field.attrs.attribute.as_ref();

            if attribute.is_some() {
                // panic!("this macro does not support the \"attribute\" attribute");
            }

            if from.is_some() && (text || attribute.is_some()) {
                panic!("the \"from\" attribute cannot be specified with the \"text\" or \"attribute\" attributes");
            }

            let selector = selector.as_ref().expect("missing \"selector\" attribute");
            let mut from: Option<Ident> =
                from.as_ref().map(|from| from.parse()).transpose().unwrap();

            let new_idx = field_selector_map.len();
            let selector_ident = field_selector_map
                .entry(selector)
                .or_insert_with(|| format_ident!("__PRIVATE__SELECTOR_{new_idx}"));

            let field_ident = &field.member;
            let field_type = &field.ty;

            if text || attribute.is_some() {
                let new_idx = type_trampoline_map.len();
                from = Some(
                    type_trampoline_map
                        .entry((selector, attribute, text, field_type))
                        .or_insert_with(|| format_ident!("__Private__TypeTrampoline{new_idx}"))
                        .clone(),
                );
            }

            let from_element_type = from
                .as_ref()
                .map(|from| quote! {#from})
                .unwrap_or_else(|| quote! {#field_type});
            let from_element_fn = quote_spanned! {field.original.span()=>
                <#from_element_type as ::scraper_util::FromElement>::from_element
            };
            let from_convert = from.as_ref().map(|_from| {
                quote_spanned! {field.original.span()=>
                    let #field_ident: #field_type = #field_ident.into();
                }
            });
            quote_spanned! {field.original.span()=>
                let #field_ident: #from_element_type =
                    #from_element_fn(#selector, __private__html.select(&#selector_ident))?;
                #from_convert
            }
        });

        quote! {
            #(#recurse)*
        }
    };

    let return_ = {
        let iter = fields.iter().map(|field| &field.member);
        quote! {
            Ok(Self {
                #(#iter,)*
            })
        }
    };

    let selectors = {
        let recurse = field_selector_map.iter().map(|(name, ident)| {
            quote! {
                static #ident: ::scraper_util::Lazy<::scraper_util::Selector> = ::scraper_util::Lazy::new(|| ::scraper_util::Selector::parse(#name).unwrap());
            }
        });

        quote! {
            #(#recurse)*
        }
    };

    let type_trampolines = {
        let recurse =
            type_trampoline_map
                .iter()
                .map(|((_selector, attribute, text, field_type), ident)| {
                    let attr_impl = match (attribute, text) {
                        (Some(attribute), false) => {
                            quote! {attribute = #attribute,}
                        }
                        (None, true) => {
                            quote! {text,}
                        }
                        (Some(_), true) => {
                            panic!(
                            "the \"text\" and \"attribute\" attributes cannot both be specified"
                        );
                        }
                        (None, false) => {
                            panic!("the \"text\" and \"attribute\" attributes were not specified");
                        }
                    };

                    quote! {
                        #[derive(::scraper_util::FromElement)]
                        struct #ident {
                            #[scraper_util(#attr_impl)]
                            text: #field_type,
                        }

                        impl Into<#field_type> for #ident {
                            fn into(self) -> #field_type {
                                self.text
                            }
                        }
                    }
                });

        quote! {
            #(#recurse)*
        }
    };

    let expanded = quote! {
        impl ::scraper_util::FromHtml for #name {
            fn from_html(__private__html: &::scraper_util::Html) -> Result<Self, ::scraper_util::FromHtmlError> {
                #type_trampolines
                #selectors
                #impl_
                #return_
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(FromElement, attributes(scraper_util))]
pub fn derive_from_element(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = &input.ident;

    let fields = match &input.data {
        Data::Struct(data) => &data.fields,
        Data::Enum(_) | Data::Union(_) => {
            return Error::new(input.span(), "expected a struct")
                .into_compile_error()
                .into()
        }
    };
    let fields = match &fields {
        Fields::Named(fields) => &fields.named,
        Fields::Unnamed(fields) => &fields.unnamed,
        Fields::Unit => {
            return Error::new(fields.span(), "unit structs are unsupported")
                .into_compile_error()
                .into()
        }
    };
    let fields = match get_fields(fields) {
        Ok(fields) => fields,
        Err(error) => return error.into_compile_error().into(),
    };

    /*
    let mut aggregate = false;
    for attr in input.attrs.iter() {
        if !attr.path.is_ident("scraper_util") {
            continue;
        }
        let meta = attr.parse_meta().unwrap();
        let meta = match meta {
            Meta::List(meta) => meta,
            _ => panic!("unexpected meta type"),
        };
        for meta in meta.nested.iter() {
            let meta = match meta {
                NestedMeta::Meta(meta) => meta,
                _ => panic!("unexpected meta type"),
            };
            if meta.path().is_ident("aggregate") {
                if aggregate {
                    panic!("\"aggregate\" attribute is specified twice");
                }

                aggregate = true;
            } else {
                panic!("unknown ident {:#?}", meta.path());
            }
        }
    }
    */

    let mut field_selector_map = HashMap::with_capacity(fields.len());
    let mut type_trampoline_map = HashMap::with_capacity(fields.len());

    let recurse = fields.iter().map(|field| {
        let attribute = &field.attrs.attribute;
        let text = field.attrs.text;
        let selector = field.attrs.selector.as_ref();

        if field.attrs.from.is_some() {
            panic!("the \"from\" attribute is not recognized by this macro");
        }

        if field.attrs.aggregate {
            panic!("the \"aggregate\" attribute is not recognized by this macro");
        }

        let field_ident = &field.member;
        let field_type = &field.ty;
        match (attribute.as_ref(), text, selector) {
            (Some(_), true, _) => {
                panic!("the \"text\" attribute is unsupported with the \"attribute\" attribute");
            }
            (Some(attribute), false, None) => {
                let from_attribute_fn = quote_spanned! {field.original.span()=>
                    <#field_type as ::scraper_util::FromAttribute>::from_attribute
                };
                quote_spanned! {field.original.span()=>
                    let #field_ident: #field_type = #from_attribute_fn(#attribute, __private__element_value.attr(#attribute))?;
                }
            }
            (None, true, None) => {
                let from_text_fn = quote_spanned! {field.original.span()=>
                    <#field_type as ::scraper_util::FromText>::from_text
                };
                quote_spanned! {field.original.span()=>
                    let #field_ident: #field_type = #from_text_fn(__private__element.text())?;
                }
            }
            (attribute, text, Some(selector)) => {
                let mut from = None;

                if text || attribute.is_some() {
                    let new_idx = type_trampoline_map.len();
                    from = Some(
                        type_trampoline_map
                            .entry((selector, attribute, text, field_type))
                            .or_insert_with(|| format_ident!("__Private__TypeTrampoline{new_idx}"))
                            .clone(),
                    );
                }

                let new_idx = field_selector_map.len();
                let selector_ident = field_selector_map
                    .entry(selector)
                    .or_insert_with(|| format_ident!("__PRIVATE__SELECTOR_{new_idx}"));

                let from_impl = from.as_ref().map(|_from| {
                    quote_spanned! {field.original.span()=>
                        let #field_ident: #field_type = #field_ident.into();
                    }
                });
                let from_fn_type = from
                    .as_ref()
                    .map(|from| quote! {#from})
                    .unwrap_or_else(|| quote! {#field_type});
                let from_element_fn = quote_spanned! {field.original.span()=>
                    <#from_fn_type as ::scraper_util::FromElement>::from_element
                };
                quote_spanned! {field.original.span()=>
                    let #field_ident: #from_fn_type = #from_element_fn(#selector, __private__element.select(&#selector_ident))?;
                    #from_impl
                }
            }
            (None, false, None) => {
                panic!("expected one of \"attribute\", \"text\", or \"selector\" attributes");
            }
        }
    });

    let impl_ = quote! {
        #(#recurse)*
    };

    let return_ = {
        let iter = fields.iter().map(|field| &field.member);
        quote! {
            Ok(Self {
                #(#iter,)*
            })
        }
    };

    let selectors = {
        let recurse = field_selector_map.iter().map(|(name, ident)| {
            quote! {
                static #ident: ::scraper_util::Lazy<::scraper_util::Selector> = ::scraper_util::Lazy::new(|| ::scraper_util::Selector::parse(#name).unwrap());
            }
        });

        quote! {
            #(#recurse)*
        }
    };

    let type_trampolines = {
        let recurse =
            type_trampoline_map
                .iter()
                .map(|((_selector, attribute, text, field_type), ident)| {
                    if *text && attribute.is_some() {
                        panic!("cannot specify both the \"text\" and \"attribute\" attributes");
                    }

                    let attribute_impl = attribute.map(|attribute| {
                        quote! {attribute = #attribute,}
                    });
                    let text_impl = if *text { Some(quote! {text,}) } else { None };

                    quote! {
                        #[derive(::scraper_util::FromElement)]
                        struct #ident {
                            #[scraper_util(
                                #attribute_impl
                                #text_impl
                            )]
                            inner: #field_type,
                        }

                        impl Into<#field_type> for #ident {
                            fn into(self) -> #field_type {
                                self.inner
                            }
                        }
                    }
                });

        quote! {
            #(#recurse)*
        }
    };

    let expanded = quote! {
        impl ::scraper_util::FromElement for #name {
            fn from_element<'a>(
                __private__selector: &str,
                mut __private__elements: impl ::core::iter::Iterator<Item = ::scraper_util::ElementRef<'a>>
            ) -> Result<Self, ::scraper_util::FromElementError> {
                #type_trampolines
                #selectors

                let __private__element = __private__elements
                    .next()
                    .ok_or_else(|| ::scraper_util::FromElementError::Missing(__private__selector.into()))?;
                let __private__element_value = __private__element
                    .value();

                #impl_

                #return_
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

#[derive(Debug)]
struct FieldAttrs {
    attribute: Option<LitStr>,
    text: bool,
    aggregate: bool,

    selector: Option<LitStr>,
    from: Option<LitStr>,
}

impl FieldAttrs {
    fn new(field: &syn::Field) -> Result<Self> {
        let mut scraper_util_attr = None;
        for attribute in field.attrs.iter() {
            let meta_items = match get_scraper_util_meta_items(attribute)? {
                Some(meta_items) => meta_items,
                None => continue,
            };

            if scraper_util_attr.is_some() {
                return Err(Error::new(
                    attribute.span(),
                    "encountered two `scraper_util` attributes, refusing to merge",
                ));
            }

            scraper_util_attr = Some(meta_items);
        }
        let scraper_util_attr = scraper_util_attr
            .ok_or_else(|| Error::new(field.span(), "missing `scraper_util` attribute"))?;

        let mut attrs = FieldAttrs {
            attribute: None,
            text: false,

            selector: None,
            from: None,

            aggregate: false,
        };

        for meta in scraper_util_attr.into_iter() {
            let meta = match meta {
                NestedMeta::Meta(meta) => meta,
                _ => {
                    return Err(Error::new(
                        meta.span(),
                        "expected a meta nested meta attribute",
                    ));
                }
            };

            match meta {
                Meta::NameValue(name_value) => {
                    if name_value.path.is_ident("attribute") {
                        if attrs.attribute.is_some() {
                            return Err(Error::new(
                                name_value.span(),
                                "duplicate \"attribute\" attribute",
                            ));
                        }

                        let value = match name_value.lit {
                            Lit::Str(value) => value,
                            _ => return Err(Error::new(name_value.span(), "unexpected literal")),
                        };

                        attrs.attribute = Some(value);
                    } else if name_value.path.is_ident("selector") {
                        if attrs.selector.is_some() {
                            return Err(Error::new(
                                name_value.span(),
                                "duplicate \"selector\" attribute",
                            ));
                        }

                        let value = match name_value.lit {
                            Lit::Str(value) => value,
                            _ => return Err(Error::new(name_value.span(), "unexpected literal")),
                        };

                        attrs.selector = Some(value);
                    } else if name_value.path.is_ident("from") {
                        if attrs.from.is_some() {
                            return Err(Error::new(
                                name_value.span(),
                                "duplicate \"from\" attribute",
                            ));
                        }

                        let value = match name_value.lit {
                            Lit::Str(value) => value,
                            _ => return Err(Error::new(name_value.span(), "unexpected literal")),
                        };

                        attrs.from = Some(value);
                    } else {
                        return Err(Error::new(name_value.path.span(), "unexpected attribute"));
                    }
                }
                Meta::Path(path) => {
                    if path.is_ident("text") {
                        if attrs.text {
                            return Err(Error::new(path.span(), "duplicate \"text\" attribute"));
                        }

                        attrs.text = true;
                    } else if path.is_ident("aggregate") {
                        if attrs.aggregate {
                            return Err(Error::new(
                                path.span(),
                                "duplicate \"aggregate\" attribute",
                            ));
                        }

                        attrs.aggregate = true;
                    } else {
                        return Err(Error::new(path.span(), "unexpected attribute"));
                    }
                }
                _ => {
                    return Err(Error::new(meta.span(), "unsupported attribute type"));
                }
            }
        }

        Ok(attrs)
    }
}

#[derive(Debug)]
struct Field<'a> {
    member: syn::Member,
    attrs: FieldAttrs,
    ty: &'a Type,
    original: &'a syn::Field,
}

fn get_fields(fields: &Punctuated<syn::Field, Comma>) -> Result<Vec<Field>> {
    fields
        .iter()
        .enumerate()
        .map(|(i, field)| {
            Ok(Field {
                member: match &field.ident {
                    Some(ident) => syn::Member::Named(ident.clone()),
                    None => syn::Member::Unnamed(i.into()),
                },
                attrs: FieldAttrs::new(field)?,
                ty: &field.ty,
                original: field,
            })
        })
        .collect::<Result<_>>()
}

fn get_scraper_util_meta_items(attribute: &Attribute) -> Result<Option<Vec<NestedMeta>>> {
    if attribute.path.is_ident("scraper_util") {
        match attribute.parse_meta()? {
            Meta::List(meta) => Ok(Some(Vec::from_iter(meta.nested))),
            bad => Err(Error::new_spanned(bad, "unrecognized attribute")),
        }
    } else {
        Ok(None)
    }
}

#[test]
fn test() {
    let t = trybuild::TestCases::new();
    t.pass("tests/test_simple_from_html.rs");
    t.pass("tests/test_simple_from_element.rs");
    t.pass("tests/test_vec_from_html.rs");
    t.pass("tests/test_from_attr_from_html.rs");
    t.pass("tests/test_text_from_element.rs");
    t.pass("tests/test_text_from_html.rs");
    t.pass("tests/test_element_from_element.rs");
    t.pass("tests/test_text_element_from_element.rs");
    t.pass("tests/test_attribute_element_from_element.rs");
    // t.pass("tests/test_aggregate_from_html.rs");
    // t.pass("tests/test_aggregate_from_element.rs");
}

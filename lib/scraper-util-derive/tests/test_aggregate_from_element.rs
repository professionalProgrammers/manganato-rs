use scraper_util::FromElement;

#[derive(Debug, scraper_util_derive::FromElement)]
#[scraper_util(aggregate)]
struct Page {
    #[scraper_util(selector = "a", attribute = "href")]
    element: String,
}

fn main() {
    let html_str = "<html><body><p><a href = \"test\">hello</a><a href = \"other\">world</a></p></body></html>";
    let html = scraper_util::Html::parse_document(html_str);
    let selector = scraper_util::Selector::parse("p").unwrap();
    let page = Page::from_element("p", html.select(&selector)).unwrap();
    
    // assert!(page.element == "test", "expected \"test\", got \"{}\"", page.element);
}
use scraper_util::FromElement;

#[derive(Debug, scraper_util_derive::FromElement)]
struct Page {
    #[scraper_util(selector = "a")]
    element: Link,
}

#[derive(Debug, scraper_util_derive::FromElement)]
struct Link {
    #[scraper_util(attribute = "href")]
    element: String,
}

fn main() {
    let html_str = "<html><body><p><a href = \"test\">hello</a></p></body></html>";
    let html = scraper_util::Html::parse_document(html_str);
    let selector = scraper_util::Selector::parse("p").unwrap();
    let page = Page::from_element("p", html.select(&selector)).unwrap();
    
    assert!(page.element.element == "test", "expected \"test\", got \"{}\"", page.element.element);
}
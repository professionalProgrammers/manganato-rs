use scraper_util::FromHtml;
use scraper_util::FromElement;
use scraper_util::FromAttribute;

#[derive(Debug, scraper_util_derive::FromHtml)]
struct Page {
    #[scraper_util(selector = "a", from = "PageElements")]
    elements: Vec<String>,
}

#[derive(Debug)]
struct PageElements {
    href: Vec<String>,
}

impl FromElement for PageElements {
    fn from_element<'a>(
        _selector: &str, 
        elements: impl Iterator<Item = scraper_util::ElementRef<'a>>
    ) -> Result<Self, scraper_util::FromElementError> {
        let href = elements
            .map(|element| {
                String::from_attribute("href", element.value().attr("href"))
            })
            .collect::<Result<_, _>>()?;
        
        Ok(Self {
            href
        })
    }
}

impl Into<Vec<String>> for PageElements {
    fn into(self) -> Vec<String> {
        self.href
    }
}

fn main() {
    let html_str = "<html><body><a href = \"hello\"><a href = \"world\"></a></body></html>";
    let html = scraper_util::Html::parse_document(html_str);
    let page = Page::from_html(&html).unwrap();
    assert!(page.elements.len() == 2);
    assert!(page.elements[0]== "hello");
    assert!(page.elements[1] == "world");
}
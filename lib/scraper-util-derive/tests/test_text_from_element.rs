use scraper_util::FromElement;

#[derive(Debug, scraper_util_derive::FromElement)]
struct Page {
    #[scraper_util(text)]
    element: String,
}

fn main() {
    let html_str = "<html><body><a href = \"test\">hello</a></body></html>";
    let html = scraper_util::Html::parse_document(html_str);
    let selector = scraper_util::Selector::parse("a").unwrap();
    let page = Page::from_element("a", html.select(&selector)).unwrap();
    
    assert!(page.element == "hello");
}
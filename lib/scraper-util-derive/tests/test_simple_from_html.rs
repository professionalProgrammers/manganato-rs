use scraper_util::FromHtml;

#[derive(Debug, scraper_util_derive::FromHtml)]
struct Page {
    #[scraper_util(selector = "a")]
    element: Link,
}

#[derive(Debug, scraper_util_derive::FromElement)]
struct Link {
    #[scraper_util(attribute = "href")]
    href: String,
}

fn main() {
    let html_str = "<html><body><a href = \"test\"></a></body></html>";
    let html = scraper_util::Html::parse_document(html_str);
    let page = Page::from_html(&html).unwrap();
    assert!(page.element.href == "test");
}
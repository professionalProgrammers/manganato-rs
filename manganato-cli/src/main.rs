/// Subcommands
mod commands;
/// Utility
pub mod util;

use anyhow::bail;

#[derive(argh::FromArgs)]
#[argh(description = "a cli to interact with manganato")]
pub struct Options {
    #[argh(subcommand)]
    subcommand: SubCommand,
}

#[derive(argh::FromArgs)]
#[argh(subcommand)]
pub enum SubCommand {
    Search(self::commands::search::Options),
    Download(self::commands::download::Options),
}

fn main() -> anyhow::Result<()> {
    let options = argh::from_env();
    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()?;
    {
        let _guard = tokio_rt.handle().enter();
        let main_handle = tokio::spawn(async_main(options));
        tokio_rt.block_on(main_handle)??;
    }
    Ok(())
}

async fn async_main(options: Options) -> anyhow::Result<()> {
    let client = manganato::Client::new();
    let ctrl_c = tokio::signal::ctrl_c();

    let mut handle = match options.subcommand {
        SubCommand::Search(options) => tokio::spawn(self::commands::search::exec(client, options)),
        SubCommand::Download(options) => {
            tokio::spawn(self::commands::download::exec(client, options))
        }
    };

    tokio::select! {
        _ = &mut handle => {},
        _ = ctrl_c => {
            handle.abort();
            // This will probably be an aborted error,
            // but the code may also have finished before the ctrl+c was processed.
            // In any case, we do not care about the output anymore.
            let _ = handle.await.is_err();
            bail!("encountered ctrl+c")
        },
    };

    Ok(())
}

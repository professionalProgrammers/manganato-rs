use anyhow::Context;
use manganato::Url;
use std::path::Path;
use std::path::PathBuf;
use tracing::error;

/// Get the last path segment of a url
pub fn get_last_url_path_segment(url: &Url) -> Option<&str> {
    url.path_segments()
        .and_then(|path_iter| path_iter.rev().next())
}

/// Remove the file at the given path, unless persisted.
///
/// This will remove the directory and all of its contents.
pub struct DropRemoveDirPath {
    path: PathBuf,
    persist: bool,
}

impl DropRemoveDirPath {
    /// Make a new [`DropRemoveDirPath`].
    pub fn new<P>(path: P) -> Self
    where
        P: AsRef<Path>,
    {
        Self {
            path: path.as_ref().into(),
            persist: false,
        }
    }

    /// Persist this directory
    pub fn persist(&mut self) {
        self.persist = true;
    }
}

impl Drop for DropRemoveDirPath {
    fn drop(&mut self) {
        if !self.persist {
            let path = std::mem::take(&mut self.path);
            tokio::spawn(async move {
                if let Err(error) = tokio::fs::remove_dir_all(path)
                    .await
                    .context("failed to remove folder")
                {
                    error!("{error:?}");
                }
            });
        }
    }
}

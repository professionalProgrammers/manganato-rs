use crate::util::get_last_url_path_segment;
use anyhow::bail;
use anyhow::Context;
use manganato::Url;
use nd_util::download_to_path;
use std::path::Path;
use std::str::FromStr;
use std::sync::Arc;
use tokio::sync::Semaphore;
use tokio::task::JoinSet;

/// The output format
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Default)]
pub enum OutputFormat {
    /// Human Output
    #[default]
    Human,

    /// Json Output
    Json,
}

impl OutputFormat {
    /// Returns true if this is OutputFormat::Human.
    pub fn is_human(&self) -> bool {
        matches!(self, Self::Human)
    }

    /// Returns true if this is OutputFormat::Json.
    pub fn is_json(&self) -> bool {
        matches!(self, Self::Json)
    }
}

impl FromStr for OutputFormat {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if input.eq_ignore_ascii_case("human") {
            return Ok(Self::Human);
        }

        if input.eq_ignore_ascii_case("json") {
            return Ok(Self::Json);
        }

        bail!("unknown format \"{input}\"");
    }
}

#[derive(argh::FromArgs)]
#[argh(
    subcommand,
    name = "download",
    description = "download manga from manganato"
)]
pub struct Options {
    #[argh(positional, description = "the manga url")]
    pub url: Url,

    #[argh(
        switch,
        short = 'v',
        long = "verbose",
        description = "whether to enable verbose output. Ignored in non-human output modes."
    )]
    pub verbose: bool,

    #[argh(
        switch,
        long = "no-progress-bar",
        description = "disable the progress bar. The progress bar will also be disabled when outputting to non-terminal targets."
    )]
    pub no_progress_bar: bool,

    #[argh(
        option,
        long = "output-format",
        default = "OutputFormat::default()",
        description = "the output format type. Accepts \"human\" and \"json\""
    )]
    pub output_format: OutputFormat,
}

/// A struct to make writing to the terminal easier
struct TermOutput {
    /// The output format
    output_format: OutputFormat,

    /// The progress bar.
    ///
    /// If this is None, there is no progress bar as it has been disabled for some reason.
    progress_bar: Option<indicatif::ProgressBar>,

    /// Whether verbose output is enabled
    verbose: bool,
}

impl TermOutput {
    fn new(
        output_format: OutputFormat,
        no_progress_bar: bool,
        verbose: bool,
        manga_chapters_len: usize,
    ) -> Self {
        // Disable progress bar if we output non-human output.
        let no_progress_bar = no_progress_bar || !output_format.is_human();

        let progress_bar = match no_progress_bar {
            false => create_progress_bar(manga_chapters_len),
            true => None,
        };

        Self {
            output_format,

            progress_bar,
            verbose,
        }
    }

    fn print_message(&self, message: &Message) -> anyhow::Result<()> {
        if self.output_format.is_json() {
            let message = serde_json::to_string(&message)?;
            self.println(format_args!("{message}"));
            return Ok(());
        }

        match message {
            Message::ChapterMetadataDownloaded { chapter_index, .. } => {
                if self.verbose {
                    let chapter_num = chapter_index + 1;
                    self.println(format_args!("chapter {chapter_num} got metadata"));
                }
            }
            Message::ChapterDownloadFinished {
                chapter_index,
                result,
            } => match result {
                Ok(()) => {
                    let chapter_num = chapter_index + 1;
                    self.println(format_args!("chapter {chapter_num} done downloading"));
                }
                Err(error) => {
                    let chapter_num = chapter_index + 1;
                    self.println(format_args!(
                        "chapter {chapter_num} failed to download: {error:?}"
                    ));
                }
            },
            Message::ImageDownloadFinished {
                chapter_index,
                image_index,
                result,
            } => match result {
                Ok(true) => {
                    let chapter_num = chapter_index + 1;
                    let image_num = image_index + 1;
                    self.println(format_args!(
                        "downloaded chapter {chapter_num}, image {image_num}",
                    ));
                }
                Ok(false) => {
                    if self.verbose {
                        let chapter_num = chapter_index + 1;
                        let image_num = image_index + 1;
                        self.println(format_args!(
                                "skipped downloading chapter {chapter_num}, image {image_num} as it already exists"
                            ));
                    }
                }
                Err(error) => {
                    let chapter_num = chapter_index + 1;
                    let image_num = image_index + 1;
                    self.println(format_args!(
                        "failed to download chapter {chapter_num}, image {image_num}: {error:?}"
                    ));
                }
            },
        }

        Ok(())
    }

    /// Print to a progress bar or the stdout if there is no progress bar.
    fn println(&self, args: std::fmt::Arguments<'_>) {
        match self.progress_bar.as_ref() {
            Some(progress_bar) => progress_bar.println(format!("{args}")),
            None => println!("{args}"),
        }
    }
}

pub async fn exec(client: manganato::Client, options: Options) -> anyhow::Result<()> {
    if options.output_format.is_human() {
        let url = options.url.as_str();
        println!("downloading '{url}'...");
    }

    let out_dir = get_last_url_path_segment(&options.url)
        .context("missing last url path segment")?
        .to_string();
    let manga = client
        .get_manga(options.url.as_str())
        .await
        .context("failed to get manga")?;

    if options.output_format.is_human() {
        let title = manga.title.as_str();
        let chapters_len = manga.chapters.len();
        let out_dir = out_dir.as_str();

        println!();
        println!("Title: {title}");
        println!("# of chapters: {chapters_len}");
        println!("Out Dir: {out_dir}");
        println!();
    }

    tokio::fs::create_dir_all(&out_dir)
        .await
        .context("failed to create out dir")?;

    let term_output = TermOutput::new(
        options.output_format,
        options.no_progress_bar,
        options.verbose,
        manga.chapters.len(),
    );
    let (tx, mut rx) = tokio::sync::mpsc::channel(1024);
    let download_semaphore = Arc::new(Semaphore::new(128));
    let mut handles = JoinSet::new();
    for (chapter_index, chapter) in manga.chapters.iter().rev().enumerate() {
        let client = client.clone();
        let download_semaphore = download_semaphore.clone();
        let chapter = chapter.clone();
        let out_dir = out_dir.clone();
        let tx = tx.clone();

        handles.spawn(async move {
            let result = async {
                let _permit = download_semaphore
                    .acquire()
                    .await
                    .context("semaphore closed");
                download_chapter(client, chapter_index, chapter, out_dir.as_ref(), tx.clone())
                    .await
                    .context("failed to download chapter")
            }
            .await;

            let msg = Message::ChapterDownloadFinished {
                chapter_index,
                result,
            };

            let _ = tx.send(msg).await.is_ok();
        });
    }
    drop(tx);

    while let Some(msg) = rx.recv().await {
        match &msg {
            Message::ChapterMetadataDownloaded { images_len, .. } => {
                if let Some(progress_bar) = term_output.progress_bar.as_ref() {
                    // It should not be possible for there to be more images than what can fit in a u64.
                    let images_len = u64::try_from(*images_len).unwrap();
                    progress_bar.inc_length(images_len);
                }
            }
            Message::ChapterDownloadFinished { .. } => {
                if let Some(progress_bar) = term_output.progress_bar.as_ref() {
                    progress_bar.inc(1);
                }
            }
            Message::ImageDownloadFinished { .. } => {
                if let Some(progress_bar) = term_output.progress_bar.as_ref() {
                    progress_bar.inc(1);
                }
            }
        }

        term_output.print_message(&msg)?;
    }

    Ok(())
}

#[derive(Debug, serde::Serialize)]
#[serde(tag = "type", content = "data")]
pub enum Message {
    /// A chapter metadata download finished
    #[serde(rename = "chapter-metadata-downloaded")]
    ChapterMetadataDownloaded {
        /// The chapter index
        chapter_index: usize,

        /// The number of images
        images_len: usize,
    },

    /// A chapter download finished
    #[serde(rename = "chapter-download-finished")]
    ChapterDownloadFinished {
        /// The chapter index
        chapter_index: usize,

        /// The result
        #[serde(serialize_with = "serialize_anyhow_result_custom")]
        result: anyhow::Result<()>,
    },

    /// An image download finished
    #[serde(rename = "image-download-finished")]
    ImageDownloadFinished {
        /// The chapter index
        chapter_index: usize,

        /// The image index
        image_index: usize,

        /// The result.
        ///
        /// This is Ok(false) if old data was reused.
        #[serde(serialize_with = "serialize_anyhow_result_custom")]
        result: anyhow::Result<bool>,
    },
}

fn serialize_anyhow_result_custom<T, S>(
    result: &anyhow::Result<T>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: serde::Serialize,
{
    use serde::Serialize;

    #[derive(serde::Serialize)]
    #[serde(tag = "type", content = "data")]
    enum JsonResult<T> {
        #[serde(rename = "ok")]
        Ok(T),

        #[serde(rename = "error")]
        Err(Vec<String>),
    }

    let result = result
        .as_ref()
        .map(JsonResult::Ok)
        .map_err(|error| JsonResult::Err(error.chain().map(|error| error.to_string()).collect()))
        .unwrap_or_else(|e| e);

    result.serialize(serializer)
}

pub async fn download_chapter(
    client: manganato::Client,
    chapter_index: usize,
    chapter: manganato::MangaChapter,
    out_dir: &Path,
    tx: tokio::sync::mpsc::Sender<Message>,
) -> anyhow::Result<()> {
    // Get the chapter
    let chapter = client
        .get_chapter(chapter.url.as_str())
        .await
        .context("failed to get chapter")?;
    tx.send(Message::ChapterMetadataDownloaded {
        chapter_index,
        images_len: chapter.images.len(),
    })
    .await
    .context("failed to send message")?;

    // Create chapter dir
    let chapter_num = chapter_index + 1;
    let chapter_dir = format!("chapter-{chapter_num}");
    let chapter_path = out_dir.join(chapter_dir.as_str());
    tokio::fs::create_dir_all(&chapter_path)
        .await
        .context("failed to create chapter dir")?;

    // Download all images
    let mut handles = JoinSet::new();
    for (image_index, image) in chapter.images.iter().enumerate() {
        let client = client.clone();
        let image = image.clone();
        let chapter_path = chapter_path.clone();
        handles.spawn(async move {
            let result = download_image(&client, image, &chapter_path)
                .await
                .with_context(|| {
                    format!(
                        "failed to download image for chapter {chapter_num}, image {}",
                        image_index + 1
                    )
                });
            Message::ImageDownloadFinished {
                chapter_index,
                image_index,
                result,
            }
        });
    }

    while let Some(msg) = handles.join_next().await {
        // TODO: How should we handle panics?
        // We can't even see what task panicked.
        if let Ok(msg) = msg {
            let _ = tx.send(msg).await.is_ok();
        }
    }

    Ok(())
}

/// Download an image
///
/// # Returns
/// Returns Ok(false) if old data was re-used.
pub async fn download_image(
    client: &manganato::Client,
    image: manganato::ChapterImage,
    chapter_path: &Path,
) -> anyhow::Result<bool> {
    let (image_name, image_extension) = parse_image_name(&image.url)?;
    let image_name = format!("{image_name}.{image_extension}");

    let path = chapter_path.join(image_name);
    match tokio::fs::metadata(&path).await {
        Ok(_metadata) => {
            // File found, do not re-download.
            Ok(false)
        }
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
            // File not found, do a download.
            download_to_path(&client.client, image.url.as_str(), &path)
                .await
                .context("failed to download to file")?;

            Ok(true)
        }
        Err(e) => {
            // Failed to get metadata,
            // the output location is probably invalid.
            Err(e).context("failed to get metadata for path")
        }
    }
}

/// Parse an image url, extracting the image number.
///
/// Image names are in the format {img-num}-{a char}, or just {img-num}.
/// We want to try to only get the img-num part here.
fn parse_image_name(url: &Url) -> anyhow::Result<(usize, &str)> {
    // Get the last path segment.
    let last_path_segment =
        get_last_url_path_segment(url).context("missing last url path segment")?;

    // Get the extension.
    let (name, extension) = last_path_segment
        .rsplit_once('.')
        .context("missing image extension")?;

    // Here, we will try to handle cases where there isn't a random trailing char.
    // We split on the first '-', and parse the first half.
    // If there is no '-', we assume name is the img-num.
    let num: usize = name
        .split_once('-')
        .map(|(num, _rest)| num)
        .unwrap_or(name)
        .parse()
        .context("failed to extract image number")?;

    Ok((num, extension))
}

/// Create a progress bar
fn create_progress_bar(size: usize) -> Option<indicatif::ProgressBar> {
    let size = size.try_into().expect("`size` cannot fit in a `u64`");
    let target = indicatif::ProgressDrawTarget::stdout();
    let progress_bar = indicatif::ProgressBar::with_draw_target(Some(size), target);
    let template =
        "[Time = {elapsed_precise} | ETA = {eta_precise} | Speed = {per_sec}] {wide_bar} {percent}%";
    let style = indicatif::ProgressStyle::default_bar()
        .template(template)
        .expect("invalid progress bar template");
    progress_bar.set_style(style);
    if progress_bar.is_hidden() {
        None
    } else {
        Some(progress_bar)
    }
}

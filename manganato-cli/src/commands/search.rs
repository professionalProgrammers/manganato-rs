use anyhow::Context;

#[derive(argh::FromArgs)]
#[argh(
    subcommand,
    name = "search",
    description = "search manganato with a query"
)]
pub struct Options {
    #[argh(positional, description = "the search query")]
    pub query: String,
}

/// Exec this subcommand
pub async fn exec(client: manganato::Client, options: Options) -> anyhow::Result<()> {
    let result = client
        .search(options.query.as_str())
        .await
        .with_context(|| format!("failed to search for `{}`", options.query))?;

    if result.entries.is_empty() {
        println!("No results for `{}`", options.query.as_str());
    } else {
        for (i, entry) in result.entries.iter().enumerate() {
            println!("{}) {}", i + 1, entry.title);
            println!("Url: {}", entry.url.as_str());
            println!();
        }
    }

    Ok(())
}
